---
title: {{ title }}
date: {{ date }}
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/_posts/{{slug}}.md
tags:
---

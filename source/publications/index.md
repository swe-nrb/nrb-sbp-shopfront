---
title: Publikationer
date: 2017-02-18 08:22:08
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/publications/index.md
---

Nedan listan publicerade informationspaket.  

Saknas ditt informationspaket? Gör en förändringsbegäran [här](https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/publications/index.md).

| Namn                                                                                                           | Status            | Källinformation                                                                                                                 |
|----------------------------------------------------------------------------------------------------------------|-------------------|---------------------------------------------------------------------------------------------------------------------------------|
|                                                                                                                |                   |                                                                                                                                 |
| __BIM Alliance__                                                                                               |                   |                                                                                                                                 |
| [Strategi för BIM i förvaltning och projekt](https://gitlab.com/swe-nrb/sbp-strategi-bim-i-staten)             | Publicerad        | -                                                                                                                               | 
| [Rollbeskrivningar](https://gitlab.com/swe-nrb/demo-infopack-rollbeskrivningar)                                | Demo              | [Ladda ner](https://gitlab.com/swe-nrb/demo-infopack-rollbeskrivningar/builds/artifacts/master/download?job=build_deliverables) |
|                                                                                                                |                   |                                                                                                                                 |
| [__Nationella riktlinjer för BIM och geodata__](http://nrb.sbplatform.se)                                      |                   | [nrb-site](https://gitlab.com/swe-nrb/nrb-site)                                                                                 |
| [Inledning](http://nrb.sbplatform.se/#/about/what-is)                                                          | Publicerad        | -                                                                                                                               |
| [Principer för informationsleveranser](http://nrb.sbplatform.se/#/about/principles)                            | Publicerad        | -                                                                                                                               |
| [Begreppslista](http://nrb.sbplatform.se/#/concepts)                                                           | Publicerad        | [sbp-begreppslista](https://gitlab.com/swe-nrb/sbp-begreppslista)                                                               |
| [Processer](http://nrb.sbplatform.se/#/processes)                                                              | Publicerad        | [sbp-processer](https://gitlab.com/swe-nrb/sbp-processer)                                                                       |
| [Metoder](http://nrb.sbplatform.se/#/methods)                                                                  | Publicerad        | [sbp-metoder](https://gitlab.com/swe-nrb/sbp-metoder)                                                                           |
| [Leveransspecifikationer](http://nrb.sbplatform.se/#/delivery-specification)                                   | Publicerad        | [sbp-leveransspecifikationer](https://gitlab.com/swe-nrb/sbp-leveransspecifikationer)                                           |

---
title: Vanliga frågor
contribute_url: 'https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/faq/index.md'
date: 2017-05-31 16:08:50
---

### Vad är Gitlab?

Gitlab är en form av projektplattform utvecklad med "öppen källkod". Gitlab är utvecklad för mjukvaruutveckling och bygger på det populära versionshanteringssystemet GIT.

[Läs mer om Gitlab](https://gitlab.com/)  
[Läs mer om GIT](https://git-scm.com/)

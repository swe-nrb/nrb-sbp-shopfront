---
title: Strukturera data med YAML
date: 2017-04-14 08:47:04
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/_posts/strukturera-data-med-yaml.md
tags:
   - Instruktioner
   - Generatorer
   - YAML
---

Vi har ofta behov av att lagra strukturerad data. Vanligt förekommande är att publicera sådan data i tabellform i antingen ett textdokument eller i en excel-fil. I denna artikel tänkte jag gå igenom ett smart alternativ till ovanstående.

## Vad är YAML?

[YAML](https://sv.wikipedia.org/wiki/YAML) är ett akronym och står för "YAML Ain't Markup Language". Till skillnad från märkspråk som [markdown](https://sv.wikipedia.org/wiki/Markdown) och [HTML](https://sv.wikipedia.org/wiki/HTML) som blandar innehåll (data) och presentation (hur innehållet visas för användaren) så är YAML ett rent dataserialiseringsformat som bara fokuserar på strukturen.

## Varför just YAML?

En av de stora styrkorna med YAML är skaparna har fokuserat på att information som lagras i formatet ska vara lätt att läsa för __både__ människa och datorer. I jämförelse med andra populära format som [JSON](https://sv.wikipedia.org/wiki/JSON) och [XML](https://sv.wikipedia.org/wiki/XML) så är YAML det format som mest liknar skriven text.

I exemplet nedan så har jag uttryckt en lista med bilar. Det är två bilar i listan med två egenskaper vardera:

### JSON

```json
[
    {
        "modell": "Ford",
        "farg": "Grön"
    },
    {
        "modell": "Volvo",
        "farg": "Röd"
    }
]
```
### XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<root>
   <element>
      <farg>Grön</farg>
      <modell>Ford</modell>
   </element>
   <element>
      <farg>Röd</farg>
      <modell>Volvo</modell>
   </element>
</root>
```

### YAML
```yaml
- farg: Grön
  modell: Ford
- farg: Röd
  modell: Volvo
```

Exemplet visar att antalet rader och "datortecken" är betydligt färre med YAML än de två andra formaten.

## Användning i informationspaket

_- Okej, så YAML är ett bra alternativ för att strukturera data men hur kan jag använda det i mitt informationspaket?_

Som en del i projektet Nationella riktlinjer BIM så har en [märkspråksgenerator](/nrb-sbp-shopfront/philosophy/generators/#Markspraksgenerator) som klarar att läsa en eller flera YAML filer och med hjälp av en mall konvertera innehållet till märkspråk (markdown). Märkspråksfilen kan sedan konverteras vidare till användbara format som .docx, .pdf eller till en hemsida (.html).

Läs mer om generatorn på [sbp-markup-generator-official](https://gitlab.com/swe-nrb/dev/sbp-markup-generator-official).

## Vill du veta mer?

Jag passar på att tipsa om ett youtube klipp som på ca 5 min går igenom grunderna i hur man kan skriva YAML.

<iframe width="560" height="315" src="https://www.youtube.com/embed/W3tQPk8DNbk" frameborder="0" allowfullscreen></iframe>

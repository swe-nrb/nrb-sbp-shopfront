---
title: Första bidrag har kommit
date: 2017-03-09 08:30:21
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/_posts/forsta-bidraget-har-kommit.md
tags:
  - Nyheter
---

Ett antal begreppsdefinitioner har nu kommit in i plattformen. Efter en inventeringsfas har arbetsgrupperna inom projektet nu börjat lägga ut definitioner av begrepp som del av Nationella Riktlinjer BIM.

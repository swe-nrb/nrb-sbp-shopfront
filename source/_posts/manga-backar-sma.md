---
title: Många bäckar små
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/_posts/manga-backar-sma.md
date: 2017-07-10 13:41:37
tags:
  - Nyheter
---

Vi har sedan årsskiftet kämpat med att beskriva vad Smart Built Platform egentligen är (eller rättare sagt vad vi vill att det ska vara). Namnet innehåller ordet "plattform" vilken antyder att det är ny "produkt" på marknaden likt Microsoft Sharepoint eller Bentleys ProjectWise. På hemsidan nämns referenser till "öppen data" och "kollaborationsverktyg" på liknande sätt som programmerare använder för att skriva program och webbsidor. Vad är det egentligen vi menar?

Smart Built Platform är varken en ny produkt eller ett verktyg för programmerare utan ska snarare ses som ett ekosystem som bygger på principen "många bäckar små".
Vid en googling på ordspårket finner man förklaringen *[ett stort antal små bidrag leder i slutändan till något som är omfattande](https://sv.wiktionary.org/wiki/M%C3%A5nga_b%C3%A4ckar_sm%C3%A5_g%C3%B6r_en_stor_%C3%A5.)* vilket jag tycker väldigt väl symboliserar ambitionen med SBP.

Behovet av en plattform adresserades konkret i projektet "Nationella riktlinjer BIM" men är egentligen lika stort för alla projekt eller organisationer som hanterar och/eller publicerar information i form av riktlinjer/råd/krav eller liknande.

> Ett stort antal små bidrag leder i slutändan till något som är omfattande

Smart Built Platform är till sin kärna ett ramverk för att strukturera och lagra information på ett "öppet" sätt. Kärnan kompletteras med ett antal tillägg som möjliggör distribution och publicering av informationen på traditionella sätt som t.ex. dokument och på nya sätt som t.ex. webbappliktioner.
Plattformen i sig är byggt på samma öppna manér och det är därmed fritt att använda, förbättra och kopiera hela eller delar av plattformen.

Baserat på ovanstående kan vi dra slutsatsen att Smart Built Platform ger oss vägledning för **hur** vi kan strukturera vår information och **verktyg** för att generera och distribuera publikationer från denna information.

---
title: Läs vårt infoblad
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/_posts/Las-vart-infoblad.md
date: 2017-10-31 13:41:01
tags:
  - Nyheter
  - Infoblad
---

Nu finns ett [infoblad](http://www.bimalliance.se/vad-aer-bim/infoblad/oeppen-plattform-skapas-foer-nationella-riktlinjer/) om vårt projekt publicerat på [BIM-alliance](http://www.bimalliance.se) webbplats.

In och läs!

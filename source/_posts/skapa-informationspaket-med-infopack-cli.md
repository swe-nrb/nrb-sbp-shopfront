---
title: Skapa informationspaket med infopack-cli
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/_posts/skapa-informationspaket-med-infopack-cli.md
date: 2017-11-10 13:21:06
tags:
  - Instruktioner
  - infopack
---

Nu har vi kommit tillräckligt långt med ramverket för att kunna publicera ett verktyg för att underlätta processen med att skapa informationspaket. Nedan följer instruktioner om hur man går tillväga.

## Installera

### 1. NodeJS

Informationspaket är en "låda med information" men också ett litet datorprogram som kan skapa dokument. Datorprogrammet behöver [NodeJS](https://nodejs.org/en/) för att fungera. Installera den nyaste versionen direkt via deras hemsida.

### 2. infopack-cli

[Infopack-cli](https://gitlab.com/sbplatform/infopack-cli) är ett "kommandoradsprogram" vilket betyder att det inte finns något grafiskt gränssnitt utan man ger programmet instruktioner genom att skriva kommandon i en s.k. terminal.

1. Öppna en terminal (Windows: kommandotolken, Mac: Terminal)
2. Testa att node är korrekt installerat genom att skriva `node -v`
    * terminalen ska då svara med ett versionsnummer
3. Installera infopack-cli genom att skriva `npm install -g infopack-cli`
    * `npm` är nodes inbyggda "pakethanterare"
    * `-g` är en instruktion att installera infopack-cli "globalt" vilket gör att programmet kan användas i alla datorns mappar
4. Bekräfta att allt är OK genom att skriva `infopack`
    * Programmets hjälpsida ska nu visas

```
Usage: infopack [options] [command]
Command line tool for managing infopacks
Options:
  -V, --version  output the version number
  -h, --help     output usage information
Commands:
  init|i [options] <name> [template]  Creates a new information package from a template.
  The package will be created in the current directory with the provided name.
```

*Exempel på hjälpsida, v0.1.1*

## Använd infopack-cli

Vi tänker oss att vi vill publicera ett informationsblad. Vi kan enkelt få en stomme genom att använda kommandot:

`$ infopack init mitt-informationsblad`

```
Copied 6 files to path: /Users/<user>/projects/mitt-informationsblad
----------------
What to do next?
1. Run: cd mitt-informationsblad && npm install
2. Update README.md
3. Hack away!
```
*Exempel på resultat, v0.1.1*

Kör `cd mitt-informationsblad && npm install` för att navigera in i mappen och installera nödvändiga paket. Glöm inte att skapa en `README.md` och beskriv ditt informationspaket.

**Klart!** Du har nu ett "tomt" informationspaket som du kan jobba i.

*Tips!*

Kör `npm start` för att köra informationspaketet.

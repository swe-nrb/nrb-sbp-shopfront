---
title: Syfte med Nationella Riktlinjer
date: 2017-05-24 18:00:00
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/_posts/syfte-med-nationella-riktlinjer.md
tags:
   - Struktur
   - Nationella Riktlinjer
   - Innehåll
---

Arbetet med Nationella Riktlinjer BIM och geodata sker samtidigt som den Digitala Plattformen utvecklas. Riktlinjerna bygger på att själva plattformen finns, men är ett arbete och en publikation i sig. Rättare sagt, det är inte en publikation, men kan med hjälp av den digitala plattformen nyttjas på olika sätt. 

## Syfte
Nationella Riktlinjer ska vara ett stöd och därmed möjliggöra en enhetlig och gemensam tillämpning av BIM och geodata i Sverige. 
- Riktlinjerna ska innefatta definitioner av digitala objekt och leveransbeskrivningar som är anpassade för hela livscykeln av den byggda miljö.
- Riktlinjer ska således möjliggöra tillämpning av BIM level 2 och 3. Definitioner av dessa nivåer ingår såklart i Riktlinjerna.
- Behovet av egna anvisningar för BIM och geodata som vi ser idag ska minimeras

## Målgrupp
- Den som levererar och/eller tar emot digital objektsorienterad information i livscykeln av den byggda miljö
- Man ska inte behöver vara en BIM- eller geodataexpert för att förstå sig på Riktlinjerna.


## Innehåll
Vi samlar på innehåll från många olika källor. Allt från internationella ramstandarder till anvisningar från enskilda organisationer.
Vi samlar på begrepp, processer, metoder och leveransspecifikationer som är relaterade till hantering av BIM och geodata. Samlingen sker med hjälp av den Digitala Plattformen. Plattformen gör att vi sedan enkelt kan hålla koll på flera olika eller snarlika versioner av samma begreppsdefinition, eller att vi kan publicera valda delar på ett enkelt sätt. Publikation kan ske i form av en traditionell skrift, men också som underlag till appar och andra digitala verktyg. Rätt så spännande och framtidssäker om vi får säga det själv.

Ni kan följa arbetet direkt genom den Digitala Plattformen och via vår arbetstavla på Trello

## Struktur
Just nu har vi påbörjat arbete med att strukturera underlaget. Vi har samlat en hel del underlag och konsten nu är att skapa en helhet av definitionerna, tabellerna, processdiagram, m.m.

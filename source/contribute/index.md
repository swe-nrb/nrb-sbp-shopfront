---
title: Bidra
date: 2017-02-18 08:22:13
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/contribute/index.md
toc: true
---

Bara det faktum att du läser denna text gör att du bidrar till "ekosystemet". Nedan har vi sammanställt hur du kan gå vidare från här.

## Skapa/förändra informationspaket

Använd gärna våra mallar för informationspaket [Länk till mallar](http://mallar.sbplatform.se/#/templates)

## Utveckla plattformen

Källinformationen kring ekosystemet ligger öppet lagrat under följande grupper

* [Nationella riktlinjer BIM](https://gitlab.com/swe-nrb) - Projektets öppna källinformation
* [Smart built platform](https://gitlab.com/sbplatform) - Informationspaketstandardens källinformation

### Prioriterade områden

Ekosystemet är under ständig utveckling och nedan listan några prioriterade områden där vi ser större utvecklingsbehov:

* Fastställa version 1.0 av informationspaketstandarden ([Länk till källinformationen](https://gitlab.com/sbplatform/sbp-informationspaket-standard))
* Utveckla en indexeringstjänst för informationspaket

Kan du bidra? Ta kontakt via Gitlab-grupperna ovan!

## Förändringsprocessen  

Det finns inga krav på att versionshantera ett informationspaket men det finns stora fördelar med att göra det. Nedan visar vi ett exempel på det arbetsflöde vi följer i [Gitlab](https://www.gitlab.com).

![Illustrationen visar projektets förändringsprocess](grafik/forandringsprocess.svg)

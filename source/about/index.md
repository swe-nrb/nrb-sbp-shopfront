---
title: Om plattformen
date: 2017-02-18 08:02:30
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/about/index.md
---

Smart built platform är en gemensam och öppen plattform för kollaborativ utveckling och förvaltning av information. Exempel på olika informationsmängder är texter, bilder eller tabeller med begrepp. Informationen kan sedan publiceras som traditionella dokument eller som "smarta" digitala lösningar såsom appar och webbapplikationer, versionshanterat och kvalitetssäkert.
Plattformen är decentraliserad till sin natur och bygger på principen "många bäckar små" vilket betyder att själva ramstandarden är väldigt generellt beskriven. Ramstandarden kompletteras sedan med tillägg för att klara ett stort antal tillämpningar.

> Visionen bakom "Smart Built Platform" är att skapa ett __modernt ekosystem__
> för att skapa, förädla och dela information

Det står vem som helst fritt att använda och bidra till plattformen.

## Bidragsgivare

Bidragsgivare (contributors) är organisationer eller projekt som varit med och bidragit till utvecklingen av plattformen.

* [Nationella riktlinjer för BIM och geodata](https://trello.com/b/dOVWrRmn/sbe-arbetspaket-riktlinje-bim) (2017 - pågående)

### Om bidragsgivarna

#### Nationella riktlinjer för BIM och geodata  

Projektet syftar till att samla och publicera branschgemensamma rekommendationer för hantering av modellbaserad information för aktörer och processer som ingår i livscykelinformationshantering av den byggda miljön. Projektet är en del av programmet Smart built environment.

__Smart built environment__

Smart Built Environment är ett strategiskt innovationsprogram för hur samhällsbyggnadssektorn kan bidra till Sveriges resa mot att bli ett globalt föregångsland som realiserar de nya möjligheter som digitaliseringen för med sig. Smart Built Environment är en del i satsningen på strategiska innovationsområden som finansieras av Vinnova, Energimyndigheten och Formas.

Smart Built Environment stöder och samordnar initiativ. Programmet arbetar med strategiska projekt och öppna utlysningar inom åtta fokusområden. Nationella Riktlinjer för BIM och geodata är ett strategiskt projekt inom fokusområdet Standardisering.

Läs mer på [programmets hemsida](http://www.smartbuilt.se/)

---
title: Plattformen
date: 2017-02-18 08:22:00
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/philosophy/index.md
---

Genom att samarbeta med information på ett liknande sätt som utvecklare samarbetar med programkod kan vi på ett öppet och kvalitetsäkert sätt samarbeta kring dokument. Vi får dessutom tillgång till kraftfulla verktyg för automatiskt distribution, versionshantering, kvalitetsäkring m.m.

> Genom att samarbeta med information på ett liknande sätt som utvecklare samarbetar med programkod kan vi på ett öppet och kvalitetsäkert sätt samarbeta kring dokument

Kärnan i plattformen är __informationspaketstandarden__. Den reglerar hur information förpackas, distribueras och modifieras. Ett paket kallas för __informationspaket__ och kan förkortas till __infopack__.

<img src="grafik/sbp_logo_200x200.png" style="height: 100px" alt="Logotyp">

Ett publikation som bär denna symbol indikerar att källinformationen är lagrad som ett informationspaket.

### Översiktskarta

![Illustration över plattformen](ekosystem.svg)

Kartan ovan illustrerar plattformen och dess ingående delar.

### Guide  

Nedan har vi sammanställt guider för dig som vill veta mer om plattformens beståndsdelar

* [Underliggande teknologier](./underlying-technologies)
* [Informationspaket](./information-package)
    * [Generatorer](./generators)

---
title: Underliggnade teknologier
date: 2017-06-09 13:32:00
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/philosophy/underlying-technologies/index.md
---

## Öppen källinformation

Inspirerat av mjukaruutveckling där begreppet "[öppen källkod](https://sv.wikipedia.org/wiki/%C3%96ppen_k%C3%A4llkod)" är allmänt vedertaget så Vill vi skapa begreppet "öppen __källinformation__". Det som karaktäriserar öppen källinformation är att den är icke properitär och att man får använda, läsa, modifiera och vidaredistribuera för den som vill.

All information är såklart inte lämpligt att släppa som öppen information men i många fall är detta det mest logiska alternativet. Typiska exempel är branchgemensam tabeller, dokument och illustrationer.

## Versionhantering

En hörnsten i att jobba "öppet" är spårbarhet och tydlighet. Det räcker inte att informationen i sig är öppen utan dialogen runt informationen behöver också vara transparent. Förändringar och "historieträdet" bör också vara tillgängliga.

Det finns väl etablerade applikationer som stödjer detta. En av de vanligaste versionshanteringsprogrammen heter [GIT](https://git-scm.com/) och det är det system som informationspaketstandarden använder för versionshantering. GIT tillsammans med webbapplikationer som [Gitlab](https://gitlab.com/) och [Github](https://github.com/).

## Webben



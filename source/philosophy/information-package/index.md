---
title: Informationspaket
date: 2017-04-13 08:23:10
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/philosophy/information-package/index.md
toc: true
is_children: true
---

Ett informationspaket är i sin enkelhet en behållare för information. Informationen är direkt läsbar men kan kan också bearbetas till nya informationsmängder via [generatorer](../generators) (t.ex. word-filer, webbsida (html), pdf). Vidare kan informationen utryckas och lagras på olika nivåer beroende på behov och komplexitet. 

> Ett informationspaket är i sin enkelhet en behållare för information

<img src="grafik/infopack_machine.svg" alt="Ett infopack lagrar öppna data och producerar publikationer">

### Anatomi  

I syfte att göra det möjligt att automatisera vissa processer vid extrahering av information så finns en fastlagd struktur som ett informationspaket måste följa. Tanken är att genom att ha tydligt ramverk så kan delar i processen bytas ut efter behov utan att påverka helheten.

![Illustration över extraheringsprocessen](./grafik/process.svg)

#### Informationssteg (mappar)  

Nedan följer en förteckning över de mappar som är definierade i ramverket.

##### Skript  

Här lagras data som utgör underlag till färdiga märkspråksfiler. Denna data är ofta strukturerad på ett sätt som är lätt för både människor och datorer att läsa (t.ex. [yaml](https://sv.wikipedia.org/wiki/YAML) eller [json](https://sv.wikipedia.org/wiki/JSON)). Vidare lagras ofta också en eller flera mallar för att tala om för märkspråksgeneratorn hur informationen ska konverteras till märkspråk.

Ramverket lämnar frihet gällande filformat och struktur här, begränsningarna kommer snarare ifrån [generatorernas](../generators) funktionalitet.

![Illustration över innehåll i skriptmappen](./grafik/skript.svg)

##### Innehall 

Här lagras textfiler i något [märkspråk](https://sv.wikipedia.org/wiki/M%C3%A4rkspr%C3%A5k) (`.md` eller `.html`). Dessa är antingen författade från grunden eller genererade via [förstegsprocessen](#Forsteg). Dessa filer kan sedan fritt konverteras till användbara informationsmängder.

Hur dessa filer hamnar i mappen är underordnat utan det viktiga är att dem är strukturerade på ett likartat sätt så att dom kan jacka i en generell extraheringsprocess.

![Illustration över innehåll i innehållsmappen](./grafik/innehall.svg)

##### Utdata  

Här lagras genererade utdata (t.ex. word-filer eller pdf:er). Efter att extraheringsprocessen är färdig så kan dessa filer (slutresultatet) distribueras fritt.

![Illustration över innehåll i utdatamappen](./grafik/utdata.svg)

#### Processer  

För att transformera information mellan informationssteg har några fasta processer definierats. Dessa processer kan automatiseras med [generatorer](../generators).

##### Försteg  

I förstegsprocessen läses strukturerat data in och bearbetas till någon form av märkspråk

##### Extrahering  

Info kommer inom kort.

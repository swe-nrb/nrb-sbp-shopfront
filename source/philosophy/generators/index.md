---
title: Generatorer
date: 2017-04-13 08:23:10
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/philosophy/generators/index.md
toc: true
is_children: true
---

En generator är ett litet program som flyttar och bearbetar information mellan olika steg i ett [informationspaket](../information-package).

### Typer

I dagsläget finns två olika typer av generatorer definierade, märkspråk och utdata generatorer.

#### Märkspråksgenerator

Denna typ av generator har till uppgift att ta någon form av strukturerat data och skapa märkspråk, ett typiskt flöde är:

1. Läsa in strukturerat data från mappen `skript`
2. Bearbeta data till märkspråk
3. Lagra resultatet i mappen `innehall`

Giltiga markup-format är för tillfället: 

* [Markdown](https://sv.wikipedia.org/wiki/Markdown)
* [HTML](https://sv.wikipedia.org/wiki/HTML)

Du kan läsa mer om vad ett märkspråk på denna [wiki-sida](https://sv.wikipedia.org/wiki/M%C3%A4rkspr%C3%A5k)

#### Utdatagenerator

En utdatagenerator läser märkspråk från mappen `innehall` och konverterar innehållet till en eller flera användbara informationsmängder

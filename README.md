# Nationella riktlinjer för BIM och geodata - Smart Built Platform  

Detta är SBP:s officella repo för webbplatsen. Den har upprättats och utvecklas
just nu av projektet "Nationella riktlinjer för BIM och geodata".

## Hemsidan  

Du hittar projektets webbplats här: http://www.sbplatform.se

## Kom i kontakt med oss 

Vi kommer gärna i kontakt med dig, du hittar oss under nedanstående länkar:

* [Rogier Jongeling (Projektledare)](https://www.linkedin.com/in/rogierjongeling)
* [Johan Asplund (Arbetspaket - Plattform)](https://www.linkedin.com/in/bimjohan)
* [Håkan Norberg (Bitr. Projektledare)](https://www.linkedin.com/in/h%C3%A5kan-norberg-2322301a)
* [Sara Beltrami (Arbetspaket - Projektering och produktion)](https://www.linkedin.com/in/sara-beltrami-64678a3/)

Saknas ditt namn? Föreslå en förändring [direkt](https://gitlab.com/swe-nrb/nrb-sbp-shopfront/edit/master/README.md)!

# Bidra  

Allt material som projektet genererar kommer att släppas som helt öppna data.
Vår förhoppning är att informationen på så sätt kan fortsätta utvecklas trots
att projektet avslutas.  
Det innebär att du är varmt välkommen att bidra till denna utveckling,
antingen med dina kloka åsikter i [diskussionsforumen](https://gitlab.com/swe-nrb/nrb-sbp-shopfront/issues)
eller via konkreta [förbättringsförslag](https://gitlab.com/swe-nrb/nrb-sbp-shopfront/merge_requests).

## Innehållet på webbplatsen

Denna webbplats är genererad med verktyget [Hexo.io](https://hexo.io/) och versionshanterad
med [GIT](https://git-scm.com/). För mer information om hur du skriver sidor/inlägg,
se [Hexos dokumentation](https://hexo.io/docs/writing.html)

*Kom ihåg - Tillsammans effektiviserar vi branschen!*  

/NRB gänget
